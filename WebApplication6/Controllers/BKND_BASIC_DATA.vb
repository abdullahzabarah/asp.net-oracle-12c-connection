Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

<Table("DEV_ENV.BKND_BASIC_DATA")>
Partial Public Class BKND_BASIC_DATA
    Public Sub New()
        BKND_BASIC_DATA1 = New HashSet(Of BKND_BASIC_DATA)()
        BKND_BASIC_DATA11 = New HashSet(Of BKND_BASIC_DATA)()
    End Sub

    Public Property ID As Decimal

    Public Property ROW_VERSION As Decimal

    <Required>
    <StringLength(500)>
    Public Property BD_NODE_NAME As String

    <StringLength(20)>
    Public Property BD_NODE_SYMBOL As String

    Public Property BD_NODE_ORDER As Decimal?

    <StringLength(500)>
    Public Property BD_NODE_VALUE As String

    <StringLength(500)>
    Public Property BD_NODE_IMAGE As String

    <StringLength(500)>
    Public Property BD_NODE_DESC As String

    Public Property PARENT As Decimal?

    Public Property CASCADE_PARENT As Decimal?

    Public Property STATUS As Decimal

    <Column(TypeName:="xmltype")>
    Public Property XML_INFO As String

    Public Property CREATED As Date

    <Required>
    <StringLength(255)>
    Public Property CREATED_BY As String

    Public Property UPDATED As Date

    <Required>
    <StringLength(255)>
    Public Property UPDATED_BY As String

    Public Overridable Property BKND_BASIC_DATA1 As ICollection(Of BKND_BASIC_DATA)

    Public Overridable Property BKND_BASIC_DATA2 As BKND_BASIC_DATA

    Public Overridable Property BKND_BASIC_DATA11 As ICollection(Of BKND_BASIC_DATA)

    Public Overridable Property BKND_BASIC_DATA3 As BKND_BASIC_DATA
End Class
