Imports System
Imports System.Data.Entity
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Linq

Partial Public Class Model1
    Inherits DbContext

    Public Sub New()
        MyBase.New("name=Model11")
    End Sub

    Public Overridable Property BKND_BASIC_DATA As DbSet(Of BKND_BASIC_DATA)

    Protected Overrides Sub OnModelCreating(ByVal modelBuilder As DbModelBuilder)
        modelBuilder.Entity(Of BKND_BASIC_DATA)() _
            .Property(Function(e) e.ID) _
            .HasPrecision(38, 0)

        modelBuilder.Entity(Of BKND_BASIC_DATA)() _
            .Property(Function(e) e.ROW_VERSION) _
            .HasPrecision(38, 0)

        modelBuilder.Entity(Of BKND_BASIC_DATA)() _
            .Property(Function(e) e.BD_NODE_NAME) _
            .IsUnicode(False)

        modelBuilder.Entity(Of BKND_BASIC_DATA)() _
            .Property(Function(e) e.BD_NODE_SYMBOL) _
            .IsUnicode(False)

        modelBuilder.Entity(Of BKND_BASIC_DATA)() _
            .Property(Function(e) e.BD_NODE_ORDER) _
            .HasPrecision(38, 0)

        modelBuilder.Entity(Of BKND_BASIC_DATA)() _
            .Property(Function(e) e.BD_NODE_VALUE) _
            .IsUnicode(False)

        modelBuilder.Entity(Of BKND_BASIC_DATA)() _
            .Property(Function(e) e.BD_NODE_IMAGE) _
            .IsUnicode(False)

        modelBuilder.Entity(Of BKND_BASIC_DATA)() _
            .Property(Function(e) e.BD_NODE_DESC) _
            .IsUnicode(False)

        modelBuilder.Entity(Of BKND_BASIC_DATA)() _
            .Property(Function(e) e.PARENT) _
            .HasPrecision(38, 0)

        modelBuilder.Entity(Of BKND_BASIC_DATA)() _
            .Property(Function(e) e.CASCADE_PARENT) _
            .HasPrecision(38, 0)

        modelBuilder.Entity(Of BKND_BASIC_DATA)() _
            .Property(Function(e) e.STATUS) _
            .HasPrecision(38, 0)

        modelBuilder.Entity(Of BKND_BASIC_DATA)() _
            .Property(Function(e) e.CREATED_BY) _
            .IsUnicode(False)

        modelBuilder.Entity(Of BKND_BASIC_DATA)() _
            .Property(Function(e) e.UPDATED_BY) _
            .IsUnicode(False)

        modelBuilder.Entity(Of BKND_BASIC_DATA)() _
            .HasMany(Function(e) e.BKND_BASIC_DATA1) _
            .WithOptional(Function(e) e.BKND_BASIC_DATA2) _
            .HasForeignKey(Function(e) e.CASCADE_PARENT)

        modelBuilder.Entity(Of BKND_BASIC_DATA)() _
            .HasMany(Function(e) e.BKND_BASIC_DATA11) _
            .WithOptional(Function(e) e.BKND_BASIC_DATA3) _
            .HasForeignKey(Function(e) e.PARENT)
    End Sub
End Class
